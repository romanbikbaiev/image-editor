import React from 'react';
import styled from 'styled-components';

import CropImage from './components/CropImage';
import Header from './components/Header';
import Footer from './components/Footer';

// Mock images
const image1 = 'https://upload.wikimedia.org/wikipedia/commons/e/ec/Gigi_Hadid_2016.jpg';
const image2 = 'https://upload.wikimedia.org/wikipedia/commons/5/5d/Gigi_Hadid_Met_Gala_2017.jpg';

const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    min-width: 700px;
`;

const CropContainer = styled.section`
    margin:auto;
`;

const AppTitle = styled.h1`
    margin-top: 0;
    margin-bottom: 30px;
    font-family: Poppins, sans-serif;
    font-size: 48px;
    font-weight: 700;
    line-height: 1.2;
    text-align: center;
    color: #171725;
`;

const Text = styled.p`
    margin-top: 18px;
    font-family: Poppins, sans-serif;
    font-size: 12px;
    font-weight: 500;
    line-height: 1.17;
    letter-spacing: 0.09px;
    text-align: center;
    color: #171725;
`;

const InnerWrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;

const App = () => (
    <PageWrapper>
        <InnerWrapper>
            <Header/>
            <CropContainer>
                <AppTitle>Crop & Compose</AppTitle>
                <CropImage image1={image1} image2={image2}/>
                <Text>Drag photos around for perfect compositon.</Text>
            </CropContainer>
        </InnerWrapper>
        <Footer/>
    </PageWrapper>
);


export default App;
