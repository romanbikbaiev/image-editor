import React from 'react';
import PropTypes from 'prop-types';

import Preview from './Preview';

const propTypes = {
    image1: PropTypes.string.isRequired,
    image2: PropTypes.string.isRequired
};

const CropImage = ({ image1, image2 }) => {
    return (
        <div>
            <Preview
                position="left"
                width={225}
                height={450}
                imageSrc={image1}
            />
            <Preview
                position="right"
                width={225}
                height={450}
                imageSrc={image2}
            />
        </div>
    );
};

CropImage.propTypes = propTypes;

export default CropImage;