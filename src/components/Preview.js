import React, { useRef, useState, useEffect } from 'react';
import styled from 'styled-components';
import { throttle } from 'lodash';


const Canvas = styled.canvas.attrs(({ position }) => ({
    position
}))`
    ${({ position }) => ({
        [`border-top-${position}-radius`] : 5,
        [`border-bottom-${position}-radius`] : 5,
        [position]: 4
    })};

    position: relative;
    border: 4px solid transparent;
    transition-property: all;
    transition-duration: 0.3s;
    
    &:hover {
        z-index: 1;
        border-color: #0062ff;
    }
`;

const Preview = ({ imageSrc, ...canvasProps }) => {
    const canvasRef = useRef(null);
    const [loaded, setLoaded] = useState(false);
    const [image, setImage] = useState(null);
    const [scale, setScale] = useState(1);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [isMouseDown, setIsMouseDown] = useState(false);
    const [mousePos, setMousePos] = useState({ x: 0, y: 0 });

    useEffect(() => {
        const image = new Image();

        setLoaded(false);
        setImage(image);
        image.src = imageSrc;
        image.onload = () => setLoaded(true);
    }, [imageSrc]);

    useEffect(() => {
        if (loaded) {
            const { width, height } = canvasRef.current;
            const scale = height / image.height * 1.5;
            setScale(scale);
            setPosition({
                x: (width / 2) - (image.width / 2) * scale,
                y: (height / 2) - (image.height / 2) * scale
            });
        }
    }, [loaded, image]);

    useEffect(() => {
        if (loaded) {
            const context = canvasRef.current.getContext('2d');
            const { width, height } = canvasRef.current;
            context.clearRect(0, 0, width, height);
            context.drawImage(image, position.x, position.y, image.width * scale, image.height * scale);
        }
    }, [loaded, image, scale, position]);

    const throttleWheel = throttle(({ deltaY }) => {
        setScale(scale => scale + deltaY / 200);
    }, 100);

    const onWheel = (e) => {
        e.persist();
        throttleWheel(e);
    };

    const onMouseDown = (e) => {
        setIsMouseDown(true);
        setMousePos({
            x: e.clientX,
            y: e.clientY
        });
    };

    const throttleMouseMove = throttle((mousePos, x, y) => {
        setPosition(position => ({
            x: position.x + x,
            y: position.y + y
        }));
        setMousePos(mousePos);
    }, 100);

    const onMouseMove = (e) => {
        e.persist();
        if (isMouseDown) {
            const moveX = e.clientX - mousePos.x;
            const moveY = e.clientY - mousePos.y;
            if (Math.abs(moveX) >= 1 || Math.abs(moveY) >= 1) {
                throttleMouseMove({ x: e.clientX, y: e.clientY }, moveX, moveY);
            }
        }
    };

    const onMouseUp = () => setIsMouseDown(false);

    return (
        <Canvas
            ref={canvasRef}
            onWheel={onWheel}
            onMouseDown={onMouseDown}
            onMouseMove={onMouseMove}
            onMouseUp={onMouseUp}
            {...canvasProps}
        />
    );
};

export default Preview;