import React from 'react';
import styled from 'styled-components';
import { ArrowBack, Clear } from 'styled-icons/material';

import { Button } from './Button';

const grayColor = '#92929d';
const grayColorHover = '#6b6b77';

const Container = styled.header`
  display: flex;
  padding: 30px 38px;
  justify-content: space-between;
  align-items: center;
`;

const ButtonBack = styled(Button)`
    display: flex;
    align-items: center;
    padding: 5px;
    color: ${grayColor};
    
    .icon {
      margin-right: 9px;
    }
    
    .icon svg {
      width: 20px;
    }
    
    &:hover,
    &:focus {
      color: ${grayColorHover}
    }
`;

const ButtonClose = styled(Button)`
    width: 37px;
    height: 37px;
    color: ${grayColor};
    background-color: transparent;
    
    svg {
      width: 25px;
    }

    span {
        position: absolute !important;
        clip: rect(1px, 1px, 1px, 1px);
        padding:0 !important;
        border:0 !important;
        height: 1px !important; 
        width: 1px !important; 
        overflow: hidden;
    }
    
    &:hover,
    &:focus {
      color: ${grayColorHover}
    }
`;


const Header = () => {
    return (
        <Container>
            <ButtonBack as="a" href="#">
                <span className="icon"><ArrowBack/></span>
                <span className="text">Back</span>
            </ButtonBack>
            <ButtonClose>
                <Clear/>
                <span>Close</span>
            </ButtonClose>
        </Container>
    );
};

export default Header;