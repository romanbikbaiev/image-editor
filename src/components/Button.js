import styled from 'styled-components';

export const Button = styled.button`
    display: inline-block;
    font-family: inherit;
    font-size: 14px;
    font-weight: 600;
    vertical-align: middle;
    letter-spacing: 0.06px;
    text-decoration: none;
    border: none;
    user-select: none;
    cursor: pointer;
    transition-property: all;
    transition-duration: 0.3s;
    
    &:active {
      opacity: 0.7;
    }
`;