import React from 'react';
import styled from 'styled-components';
import { CropFree, Crop, FlipToFront, Input, ArrowRightAlt } from 'styled-icons/material';

import { Button } from './Button';


const ProgressBar = styled.div`
    width: 30%;
    height: 3px;
    background-image: linear-gradient(90deg, #0062ff, darkblue);
`;

const FooterContent = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 10px 30px;
    background-color: #edf3f9;
    
    .logo {
        svg {
            width: 24px;
            height: 24px;
            margin-right: 12px;
            fill: #0062ff;
        }
    
        font-family: Poppins, sans-serif;
        font-size: 14px;
        font-weight: 600;
        line-height: 1;
        letter-spacing: 0.1px;
        color: #171725;
    }
    
    .buttons-wrapper {
       display: flex;
    }
`;

const ControlButton = styled(Button)`
  width: 60px;
  height: 60px;
  margin-right: 14.5px;
  border-radius: 5px;
  background-color: #fff;
  color: #171725;
  text-align: center;
 
  &:last-child {
    margin-right: 0;
  }
  
  svg {
      display: block;
      margin-left:auto;
      margin-right:auto;
      width: 30px;
      margin-bottom: 5px;
  }

  span {
      display: block;
      font-family: Roboto, sans-serif;
      font-size: 9px;
      font-weight: bold;
      line-height: 1.33;
      letter-spacing: 0.3px;
  }
  
  &:hover {
      background-color:#ccc;
  }
`;

const FinishButton = styled(Button)`
  padding: 10px;
  margin-right: 32px;
  letter-spacing: 0.06px;
  text-align: center;
  color: #92929d;
  background-color:#fff;
  border-radius: 5px;
  
   &:hover,
   &:focus {
      background-color:#ccc;
  }
`;

const NextButton = styled(FinishButton)`
    display: flex;
    align-items: center;
    margin-right: 0;
    padding: 8px 10px;
    color: #fff;
    background-color:#1962ff;
    
    span {
        margin-right: 30px;
    }
    
    svg {
        width: 20px;
    }
    
    &:hover,
    &:focus {
      background-color: blue;
    }
`;

const Footer = () => {
    return (
        <footer>
            <div>
                <ProgressBar/>
            </div>
            <FooterContent>
                <div className="logo">
                    <CropFree/>
                    <span>Crop & Compose</span>
                </div>
                <div>
                    <ControlButton>
                        <Crop/>
                        <span>Crop</span>
                    </ControlButton>
                    <ControlButton>
                        <FlipToFront/>
                        <span>Mirror</span>
                    </ControlButton>
                    <ControlButton>
                        <Input/>
                        <span>Flip</span>
                    </ControlButton>
                </div>
                <div className="buttons-wrapper">
                    <FinishButton>Finish & Share</FinishButton>
                    <NextButton>
                        <span>Next</span>
                        <ArrowRightAlt/>
                    </NextButton>
                </div>
            </FooterContent>
        </footer>
    );
};

export default Footer;